#include <iostream>
#include <string>
class Linked_list {
public:
    Linked_list()
    {
        head = nullptr;
    }
    void add(int val, int i) //this method adds nodes by index
    {
        if (i < 0)
        {
            throw std::exception("Index is negative!");
        }
        if (head == nullptr)
        {
            head = new Node();
            head->value = val;
            head->next = nullptr;
            return;
        }
        Node* current = head;
        if (i == 0)
        {
            head = new Node();
            head->value = val;
            head->next = current;
            return;
        }
        int count = 1;
        while (current->next != nullptr && i > count)
        {
            current = current->next;
            count++;
        }
        Node* node = new Node();
        node->value = val;
        node->next = current->next;
        current->next = node;
    };
    void pop() // this method deletes node in the end of the list
    {
        Node* current = head;
        if (current == nullptr)
        {
            throw std::exception("List is empty!");
        }
        else if (current->next == nullptr)
        {
            delete head;
            head = nullptr;
            return;
        }
        while (current->next->next != nullptr)
        {
            current = current->next;
        }
        Node* node = current->next;
        current->next = nullptr;
        delete node;
    };
    int at(int i) // this method returns value at certain index
    {
        if (i < 0)
        {
            throw std::exception("Index is negative.");
        }
        int count = 0;
        Node* current = head;
        while (current->next != nullptr && i > 0 && count < i)
        {
            current = current->next;
            count++;
        }
        if (current->next == nullptr && i > count)
        {
            throw std::exception("Index is out of list.");
        }
        return current->value;
    };
    void pop(int i) // this method deletes node at certain index
    {
        if (is_empty())
        {
            throw std::exception("List is empty!");
        }
        if (i < 0)
        {
            throw std::exception("Index is negative!");
        }
        int count = 0;
        Node* current = head;
        if (i == 0)
        {
            Node* node = current;
            head = current->next;
            delete node;
            return;
        }
        while (current->next != nullptr && i > count + 1)
        {
            current = current->next;
            count++;
        }
        if (current->next == nullptr && i > count)
        {
            throw std::invalid_argument("Index is out of list.");
        }
        Node* node = current->next;
        current->next = current->next->next;
        delete node;
    };
    bool is_empty() // this method checks if list is empty
    {
        if (head == nullptr)
        {
            return true;
        }
        return false;
    }
    void print()
    {
        if (is_empty())
        {
            throw std::exception("List is empty.");
        }
        int count = 0;
        Node* current = head;
        while (current->next != nullptr)
        {
            std::cout << current->value << " ";
            current = current->next;
            count++;
        }
        std::cout << current->value << " ";
    }
private:
    struct Node {
        int value;
        Node* next;
    };
    Node* head;
    int len = 0;
};
    



int main()
{
    std::string str;
    Linked_list lst;
    std::cout << "COMMANDS:" << std::endl << "1. add value at index" << std::endl << "2. delete value at the end" <<
        std::endl << "3. value at index" << std::endl << "4. delete value at index" << std::endl <<
        "5. check if list is empty" << std::endl << "6. print list" << std::endl << "7. end programme" << std::endl;
    do
    {
        std::getline(std::cin, str);
        if (str == "add value at index")
        {
            int val;
            std::cin >> val;
            int i;
            std::cin >> i;
            try {
                lst.add(val, i);
            }
            catch (std::exception &ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "delete value at the end")
        {
            try
            {
                lst.pop();
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "value at index")
        {
            int i;
            std::cin >> i;
            try
            {
                std::cout << lst.at(i) << std::endl;
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "delete value at index")
        {
            int i;
            std::cin >> i;
            try
            {
                lst.pop(i);
            }
            catch (std::exception& ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
        else if (str == "check if list is empty")
        {
            if (lst.is_empty() == true)
            {
                std::cout << "list is empty";
            }
            else
            {
                std::cout << "list is not empty";
            }
        }
        else if (str == "print list")
        {
            try
            {
                lst.print();
            }
            catch (std::exception & ex)
            {
                std::cout << ex.what() << std::endl;
            }
        }
    } while (str != "end programme");
    /*std::cout << lst.is_empty();
    try {
        lst.pop();
        for (int i = 0; i < 4; i++)
        {
            std::cout << lst.at(i) << " ";
        }
    }
    catch (std::invalid_argument& ex) {
        std::cout << ex.what();
    }*/
    system("pause");
}